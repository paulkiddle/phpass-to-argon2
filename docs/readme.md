# phpass-to-argon2

Methods for checking passwords against phpass or argon2 hashes,
and upgrading then to argon2 where needed.

```javascript
import verify, { hash } from 'phpass-to-argon2';

/**
 * An example login function using phpass-to-argon2
 * @param user The user object to verify against
 * @param password The password submitted by the login form
 */
export default async function login(user, password) {
	return verify(
		// Supply the password and the hash to verify against
		user.passwordHash,
		password,

		// Supply a function for updating the hash when needed
		async newHash => {
			user.passwordHash = newHash;
			await user.save();
		}
	)
}

// We also export the argon2 hash method
// for setting the password normally.
export async function updatePassword(user, newPassword) {
	user.passwordHash = await hash(newPassword);
	await user.save();
}
```
