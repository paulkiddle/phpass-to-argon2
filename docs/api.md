
## Modules

<dl>
<dt><a href="#module_phpass-to-argon2">phpass-to-argon2</a></dt>
<dd></dd>
<dt><a href="#module_verify">verify</a></dt>
<dd><p>Verify a password against a phpass or argon2 hash.
If the <code>update</code> argument is passed, it will be called when a password hash needs updating from phpass to argon2</p>
</dd>
<dt><a href="#module_hash">hash</a> ⇒ <code>string</code></dt>
<dd><p>Hash a password using argon2</p>
</dd>
<dt><a href="#module_needsUpdate">needsUpdate</a> ⇒ <code>boolean</code></dt>
<dd><p>Check if a hash needs to be updated</p>
</dd>
</dl>

<a name="module_phpass-to-argon2"></a>

## phpass-to-argon2
<a name="module_verify"></a>

## verify
Verify a password against a phpass or argon2 hash.
If the `update` argument is passed, it will be called when a password hash needs updating from phpass to argon2


| Param | Type | Description |
| --- | --- | --- |
| hash | <code>string</code> | The hash to compare with |
| password | <code>string</code> | The password to compare |
| [update] | <code>function</code> | A function to execute when the stored hash needs updating, taking the new hash as the first argument |
| [options] | <code>object</code> | The [argon2 options object](https://github.com/ranisalt/node-argon2/wiki/Options) |

<a name="module_hash"></a>

## hash ⇒ <code>string</code>
Hash a password using argon2

**Returns**: <code>string</code> - The hashed password

| Param | Type | Description |
| --- | --- | --- |
| password | <code>string</code> | The password to be hashed |
| options | <code>object</code> | The [argon2 options object](https://github.com/ranisalt/node-argon2/wiki/Options) |

<a name="module_needsUpdate"></a>

## needsUpdate ⇒ <code>boolean</code>
Check if a hash needs to be updated


| Param | Type | Description |
| --- | --- | --- |
| hash | <code>string</code> | The hash to check |

