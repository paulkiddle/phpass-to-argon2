import verify, {hash} from '../src/index.js';
import {jest} from '@jest/globals';

test('verify', async ()=>{
	expect(await verify('$2a$08$jaonfVfT3NX4EW8do7eJou04yZ1MYhqnZbya2SuA4EUFWYkKtPY8C', 'password')).toBe(true);
	expect(await verify('$argon2i$v=19$m=16,t=2,p=1$U0JOUjZhRHF0eTRFQ0l0ZA$dr4x4dU1qnZ3HzV4dnZGzQ', 'password')).toBe(true);
});

test('verify and update', async ()=>{
	const update = jest.fn();
	expect(await verify('$2a$08$jaonfVfT3NX4EW8do7eJou04yZ1MYhqnZbya2SuA4EUFWYkKtPY8C', 'password', update)).toBe(true);
	expect(update).toHaveBeenCalledWith(expect.stringMatching(/^\$argon2/));
});

test('hash', async()=>{
	expect(await hash('new-password')).toMatch(/^\$argon2/);
});
