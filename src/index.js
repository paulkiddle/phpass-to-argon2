/**
 * @module phpass-to-argon2
 * @exports default
 **/

import p from 'phpass';
import argon2 from 'argon2';

const a2='$argon2';

const {PasswordHash} = p;

const phpass = new PasswordHash();

function isPhpass(hash){
	return !hash.startsWith(a2);
}

/**
 * Verify a password against a phpass or argon2 hash.
 * If the `update` argument is passed, it will be called when a password hash needs updating from phpass to argon2
 * @module phpass-to-argon2
 * @function verify
 * @exports module:phpass-to-argon2
 *
 * @param {string} hash The hash to compare with
 * @param {string} password The password to compare
 * @param {function} [update] A function to execute when the stored hash needs updating, taking the new hash as the first argument
 * @param {object} [options] The [argon2 options object](https://github.com/ranisalt/node-argon2/wiki/Options)
 */
export default async function verify(hash, password, update, options) {
	if(isPhpass(hash)){
		const valid = await phpass.checkPassword(password, hash);
		if(valid && update) {
			await update(await argon2.hash(password, options));
			return true;
		}

		return valid;
	}else{
		return argon2.verify(hash, password);
	}
}

/**
 * Hash a password using argon2
 * @param {string} password The password to be hashed
 * @param {object} options The [argon2 options object](https://github.com/ranisalt/node-argon2/wiki/Options)
 * @returns {string} The hashed password
 *
 * @module phpass-to-argon2
 * @function hash
 * @exports module:phpass-to-argon2
 *
 */
export async function hash(password, options){
	return argon2.hash(password, options);
}

/**
 * Check if a hash needs to be updated
 * @function needsUpdate
 * @exports module:phpass-to-argon2
 * @param {string} hash The hash to check
 * @returns {boolean}
 */
export { isPhpass as needsUpdate };
